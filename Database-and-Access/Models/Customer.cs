﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database_and_Access.Models
{
    public class Customer
    {
        public int id { get; set; }
        public string? firstName { get; set; }
        public string? lastName { get; set; }
        public string? country { get; set; }
        public string? postalCode { get; set; }
        public string? phoneNumber { get; set; }
        public string? email { get; set; }

    }
}
