﻿using Database_and_Access.Repositories;
using Database_and_Access.Models;


public class Program
{
    public static void Main(string[] args)
    {
        ICustomerRepository repository = new CustomerRepository();

        TestGetFavouriteGenre(repository);
    }

    public static void TestUpdateCustomer(ICustomerRepository rep)
    {
        //TODO:
        // - Fix for null values

        Customer customer = new Customer()
        {
            id = 1,
            //firstName = "Experis",
            lastName = "Academy",
            country = "Sweden",
            postalCode = "1337",
            phoneNumber = "1337-1337",
            email = "Onyon@tor.ded"

        };
        if (rep.UpdateCustomer(customer)) { Console.WriteLine("Its done"); } else { Console.WriteLine("Failure :c"); }
    }

    public static void TestGetCustomerByCountry(ICustomerRepository rep)
    {
        PrintCustomersCountry(rep.NumberOfCustomerInCountries());
    }
    public static void TestGetFavouriteGenre(ICustomerRepository rep)
    {
        Customer test = new Customer() { id = 12 };
        PrintCustomerGenres(rep.DisplayMostPopularGenre(test));
    }

    public static void TestGetOffset(ICustomerRepository rep)
    {
        PrintCustomers(rep.GetCustomerPage(4, 5));
    }

    public static void TestGetOneByName(ICustomerRepository rep)
    {
        PrintCustomer(rep.GetCustomerByName("John"));
    }

    public static void TestGetoneById(ICustomerRepository rep)
    {
        PrintCustomer(rep.GetCustomerById("1"));
    }

    public static void TestGetAll(ICustomerRepository rep)
    {
        PrintCustomers(rep.GetAllCustomers());
    }
    public static void TestAddCustomer(ICustomerRepository rep)
    {
        Customer customer = new Customer()
        {
            firstName = "Axel",
            lastName = "Yxa",
            country = "Sweden",
            postalCode = "11425",
            phoneNumber = "0000-8888",
            email = "dotanerd@gmail.com"
        };
        if (rep.AddNewCustomer(customer))
        {
            Console.WriteLine("Added");
        }
        else
        {
            Console.WriteLine("FAIL!");
        }
    }
    public static void TestGetTopSpenders(ICustomerRepository rep)
    {
        PrintCustomerSpenders(rep.HighestSpenders());
    }
    public static void PrintCustomers(IEnumerable<Customer> customers)
    {
        foreach(Customer customer in customers)
        {
            PrintCustomer(customer);
        }
    }
    public static void PrintCustomer(Customer customer)
    {
        Console.WriteLine($"---- {customer.id} {customer.firstName} {customer.lastName} {customer.country} {customer.postalCode} {customer.phoneNumber} {customer.email} ----");
    }

    public static void PrintCustomerCountry(CustomerCountry customer)
    {
        Console.WriteLine($"--- {customer.Country} {customer.NumberofUsers}");
    }

    public static void PrintCustomersCountry(IEnumerable<CustomerCountry> customers)
    {
        foreach (CustomerCountry customer in customers)
        {
            PrintCustomerCountry(customer);
        }
    }
    public static void PrintCustomerSpender(CustomerSpender customer)
    {
        Console.WriteLine($"--- {customer.Id} {customer.FirstName} {customer.LastName} {customer.TotalSpending}");
    }

    public static void PrintCustomerSpenders(IEnumerable<CustomerSpender> customers)
    {
        foreach (CustomerSpender customer in customers)
        {
            PrintCustomerSpender(customer);
        }
    }
    public static void PrintCustomerGenre(CustomerGenre customer)
    {
        Console.WriteLine($"--- {customer.Id} {customer.FirstName} {customer.LastName} {customer.Genre}");
    }

    public static void PrintCustomerGenres(IEnumerable<CustomerGenre> customers)
    {
        foreach (CustomerGenre customer in customers)
        {
            PrintCustomerGenre(customer);
        }
    }

}