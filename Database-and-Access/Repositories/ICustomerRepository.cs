﻿using Database_and_Access.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database_and_Access.Repositories
{
    public interface ICustomerRepository
    {
        public Customer GetCustomerById(string ID);
        public Customer GetCustomerByName(string Name);
        public List<Customer> GetAllCustomers();
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public List<CustomerCountry> NumberOfCustomerInCountries();
        public List<CustomerSpender> HighestSpenders();
        public List<CustomerGenre> DisplayMostPopularGenre(Customer customer);

        public List<Customer> GetCustomerPage(int limit, int offset);

    }
}
