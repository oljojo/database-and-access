﻿using Database_and_Access.Models;
using Microsoft.Data.SqlClient;
using System.Data.SqlTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database_and_Access.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Adds a customer to the Database, based on a Customer object
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>True if the new customer gets added to DB</returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool result = false;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phonenumber, @Email)";
            try
            {
                using (SqlConnection conn = new(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.firstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.lastName);
                        cmd.Parameters.AddWithValue("@Country", customer.country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.postalCode);
                        cmd.Parameters.AddWithValue("@PhoneNumber", customer.phoneNumber);
                        cmd.Parameters.AddWithValue("@Email", customer.email);
                        result = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Gets the most popular genre fo a Customer based on the ID of the customer object
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>A list of either on or two CustomerGenre objects depending on if the Customer has two or one favourite genre</returns>
        public List<CustomerGenre> DisplayMostPopularGenre(Customer customer)
        {
            List<CustomerGenre> customerList = new();
            string sql = "SELECT TOP(2) Customer.CustomerId, Customer.FirstName, Customer.LastName, Genre.Name, COUNT(Genre.Name) " +
                "FROM Customer " +
                "INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                "INNER JOIN InvoiceLine ON InvoiceLine.InvoiceId = Invoice.InvoiceId " +
                "INNER JOIN Track ON Track.TrackId = InvoiceLine.TrackId " +
                "INNER JOIN Genre ON Genre.GenreId = Track.GenreId " +
                "WHERE Customer.CustomerId=@ID " +
                "GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName, Customer.Country, Customer.PostalCode, Customer.Phone, Customer.Email, Genre.GenreId, Genre.Name " +
                "ORDER BY COUNT(Genre.Name) DESC";

            try
            {
                using (SqlConnection conn = new(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", customer.id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre newCustomer = new();
                                if (!reader.IsDBNull(0))
                                    newCustomer.Id = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    newCustomer.FirstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    newCustomer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                    newCustomer.Genre = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                    newCustomer.GenreCount = reader.GetInt32(4);

                                customerList.Add(newCustomer);
                            }
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (customerList[1].GenreCount == customerList[0].GenreCount)
            {
                return customerList;
            }
            else
            {

                return new List<CustomerGenre>() { customerList[0] };
            }

        }
        /// <summary>
        /// Connects to the database, as written in "ConnectionStringHelper".
        /// Uses a "SELECT" query to get all customers. Checks if each query response is null before reading them.
        /// </summary>
        /// <returns>all Customers as a list of type "Customer"</returns>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection conn = new(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer newCustomer = new();
                                if (!reader.IsDBNull(0))
                                    newCustomer.id = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    newCustomer.firstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    newCustomer.lastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                    newCustomer.country = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                    newCustomer.postalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                    newCustomer.phoneNumber = reader.GetString(5);
                                if (!reader.IsDBNull(6))
                                    newCustomer.email = reader.GetString(6);
                                customerList.Add(newCustomer);
                            }
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }
        /// <summary>
        /// Gets a single customer based on ID in the database
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>A single Customer from class "Customer"</returns>
        public Customer GetCustomerById(string ID)
        {
            Customer newCustomer = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId=@id";
            try
            {
                using (SqlConnection conn = new(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@id", ID);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                if (!reader.IsDBNull(0))
                                    newCustomer.id = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    newCustomer.firstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    newCustomer.lastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                    newCustomer.country = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                    newCustomer.postalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                    newCustomer.phoneNumber = reader.GetString(5);
                                if (!reader.IsDBNull(6))
                                    newCustomer.email = reader.GetString(6);

                            }
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return newCustomer;
        }
        /// <summary>
        /// Searches for a single customer based on a name input. Uses the "LIKE" clause to get a partial match.
        /// </summary>
        /// <param name="Name"></param>
        /// <returns>Single Customer</returns>
        public Customer GetCustomerByName(string Name)
        {
            Customer newCustomer = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE @name";
            try
            {
                using (SqlConnection conn = new(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@name", "%" + Name + "%");
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                if (!reader.IsDBNull(0))
                                    newCustomer.id = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    newCustomer.firstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    newCustomer.lastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                    newCustomer.country = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                    newCustomer.postalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                    newCustomer.phoneNumber = reader.GetString(5);
                                if (!reader.IsDBNull(6))
                                    newCustomer.email = reader.GetString(6);

                            }
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return newCustomer;
        }

        public List<Customer> GetCustomerPage(int limit, int offset)
        {
            List<Customer> customerList = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @offset ROWS FETCH NEXT @limit ROWS ONLY";
            try
            {
                using (Microsoft.Data.SqlClient.SqlConnection conn = new(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@limit", limit);
                        cmd.Parameters.AddWithValue("@offset", offset);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer newCustomer = new();
                                if (!reader.IsDBNull(0))
                                    newCustomer.id = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    newCustomer.firstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    newCustomer.lastName = reader.GetString(2);
                                if (!reader.IsDBNull(3))
                                    newCustomer.country = reader.GetString(3);
                                if (!reader.IsDBNull(4))
                                    newCustomer.postalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5))
                                    newCustomer.phoneNumber = reader.GetString(5);
                                if (!reader.IsDBNull(6))
                                    newCustomer.email = reader.GetString(6);
                                customerList.Add(newCustomer);
                            }
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }
        /// <summary>
        /// Gets the highest spender starting from highest to lowest
        /// </summary>
        /// <returns>List of CustomerSpender</returns>
        public List<CustomerSpender> HighestSpenders()
        {
            List<CustomerSpender> customerList = new();
            string sql = "SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, SUM(Invoice.Total) AS TotalSpending " +
                "FROM Customer INNER JOIN Invoice ON Customer.CustomerId = Invoice.CustomerId " +
                "GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName " +
                "ORDER BY TotalSpending DESC";

            try
            {
                using (SqlConnection conn = new(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender newCustomer = new();
                                if (!reader.IsDBNull(0))
                                    newCustomer.Id = reader.GetInt32(0);
                                if (!reader.IsDBNull(1))
                                    newCustomer.FirstName = reader.GetString(1);
                                if (!reader.IsDBNull(2))
                                    newCustomer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(2))
                                    newCustomer.TotalSpending = (double)reader.GetDecimal(3);

                                customerList.Add(newCustomer);
                            }
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }
        /// <summary>
        /// Gets the Number of Customers in each avalible country.
        /// </summary>
        /// <returns>List of CustomerCountry starting from most to least frequent</returns>
        public List<CustomerCountry> NumberOfCustomerInCountries()
        {
            List<CustomerCountry> customerList = new();
            string sql = "Select Country, COUNT(Country) from Customer GROUP BY Country ORDER BY COUNT(Country) DESC";
            try
            {
                using (SqlConnection conn = new(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry newCustomer = new();
                                if (!reader.IsDBNull(0))
                                    newCustomer.Country = reader.GetString(0);
                                if (!reader.IsDBNull(1))
                                    newCustomer.NumberofUsers = reader.GetInt32(1);
                                customerList.Add(newCustomer);
                            }
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customerList;
        }
        /// <summary>
        /// Attempts to connect to database and update a line based on parameters given 
        /// in the function TestUpdateCustomer in Program.cs
        /// </summary>
        /// <param name="customer"></param>
        /// <returns>True or false, depending on success of query</returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool result = false;
            string sql = "UPDATE Customer " +
                "Set FirstName=@FirstName, LastName=@LastName, Country=@Country, PostalCode=@PostalCode, Phone=@Phone, Email=@Email " +
                "WHERE CustomerId=@ID";
            try
            {
                using (SqlConnection conn = new(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@ID", customer.id);
                        if(customer.firstName != null)
                            cmd.Parameters.AddWithValue("@FirstName", customer.firstName);
                        else
                            cmd.Parameters.AddWithValue("@FirstName", "null");
                        if (customer.lastName != null)
                            cmd.Parameters.AddWithValue("@LastName", customer.lastName);
                        else
                            cmd.Parameters.AddWithValue("@LastName", "null");
                        if (customer.country != null)
                            cmd.Parameters.AddWithValue("@Country", customer.country);
                        else
                            cmd.Parameters.AddWithValue("@Country", "null");
                        if (customer.postalCode != null)
                            cmd.Parameters.AddWithValue("@PostalCode", customer.postalCode);
                        else
                            cmd.Parameters.AddWithValue("@PostalCode", "null");
                        if (customer.phoneNumber != null)
                            cmd.Parameters.AddWithValue("@Phone", customer.phoneNumber);
                        else
                            cmd.Parameters.AddWithValue("@Phone", "null");
                        if (customer.email != null)
                            cmd.Parameters.AddWithValue("@Email", customer.email);
                        else
                            cmd.Parameters.AddWithValue("@Email", "null");
                        result = cmd.ExecuteNonQuery() > 0 ? true : false;
                        {
                        }
                    }
                }
            }

            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }
    }
}
