USE SuperheroesDb;

CREATE TABLE Superhero (
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(100) null,
	Alias varchar(100) null,
	Origin varchar(100) null
)

CREATE TABLE Assistant(
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(100) null
)

CREATE TABLE Power(
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(100) null,
	Description varchar(100) null
)
