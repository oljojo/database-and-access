USE SuperheroesDb;


Insert into Power(Name, Description)

Values ('Superspeed', 'Run very fast'),
('Reality bending', 'Create things from thin air'),
('Dropkick', 'Jump from a height and land on someone feet first'),
('Invisibility', 'Discover someone has attempted to plant a hidden microphone in your employers building')

insert into SuperheroPower(SuperheroId, PowerId)
Values (1, 1),
(2, 1),
(2, 2)
