USE SuperheroesDb;

INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Invincible', 'Mark', 'America'),
('Superman', 'Kal-El', 'Krypton'),
('Batman', 'Bruce Wayne', 'Gotham')
