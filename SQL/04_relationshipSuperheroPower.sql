USE SuperheroesDb;

CREATE TABLE SuperheroPower (
	SuperheroId int,
	PowerId int,
	FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id),
	FOREIGN KEY (PowerId) REFERENCES Power(Id),
	PRIMARY KEY (SuperheroId, PowerId)
)