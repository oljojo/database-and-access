# Assignment 5 - SQL scripts & Database access

This project consists of two separate components. The SQL folder contains 9 SQL script files that let's the user create a small database that stores superheroes, their assistants as well as their powers.

The Database-and-access folder contains a C# project aimed at performing various tasks against a database, including getting data in various forms, inserting new data, updating and deleting rows as necessary.

## Table of Contents

- [Clone](#clone)
- [Install](#install)
- [Usage](#usage)
- [Start](#start)
- [Contributors](#contributors)

## Clone

You can clone this repository for your own uses through the following command in your terminal.
```
$cd (Chosen folder)
$git clone https://gitlab.com/oljojo/database-and-access.git
```

## Install

To run the database access software, the user needs to install the addon Microsoft SqlClient. The user will also need SQL Express along with a database manager of their choice to run the SQL scripts in the SQL folder.

To utilize the database-and-access project, the user needs to run a separate SQL script (not provided) to start up a database, as well as modify the builder.Datasource in ConnectionStringHelper.cs file. 
```

        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder builder = new();
            builder.DataSource = [INSERT DATASOURCE HERE];
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;
            builder.TrustServerCertificate = true;
            return builder.ConnectionString;
        }
```
## Usage

The goal with this software is to let the user create smaller database to play around with and view the data within, as well as use a software that let's the user access data and manipulate a separate database.

## Start

After downloading the code, open the solution (Database-And-Access.sln) with visual studio code. Afterwards, you can run the software simply through Visual Studio.

## Contributors

Erik - @ermo1222

Olof - @oljojo 
